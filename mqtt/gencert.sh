#!/bin/bash

# Details about this file can be found in README.md

# Create the different certificates and keys
openssl genrsa -out ca.key 2048
openssl req -new -x509 -days 825 -key ca.key -out ca.crt -subj '/C=US/ST=MD/L=Gaithersburg/O=NIST/OU=SSD/CN=*.iot.ssd.nist.gov'
openssl genrsa -out server.key 2048
openssl req -new -out server.csr -key server.key -subj '/C=US/ST=MD/L=Gaithersburg/O=NIST/OU=SSD/CN=mqtt-broker.iot.ssd.nist.gov'
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt -days 825

# Move certificates
sudo rm -f /etc/mosquitto/certs/server.*
sudo rm -f /etc/mosquitto/ca_certificates/ca.*
sudo cp server.crt server.key -t /etc/mosquitto/certs
sudo cp ca.crt -t /etc/mosquitto/ca_certificates

# This could be removed. I use it to download cert using FTP.
sudo rm -f /home/pi/ftp/files/ca.*
cp ca.crt /home/pi/ftp/files

# Append broker details to the Mosquitto configuration file
sudo tee -a /etc/mosquitto/mosquitto.conf > /dev/null <<EOF
port 8883
cafile /etc/mosquitto/ca_certificates/ca.crt
keyfile /etc/mosquitto/certs/server.key
certfile /etc/mosquitto/certs/server.crt
tls_version tlsv1.2
EOF

# Restart the broker 
sudo systemctl restart mosquitto