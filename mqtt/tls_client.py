import time
import paho.mqtt.client as mqtt

BROKER = 'mqtt.broker'
PORT = 8883
conn_flag = True


def on_connect(client, userdata, flags, rc):
    global conn_flag
    conn_flag = True
    print(f"connected {conn_flag}")


def on_log(client, userdata, level, buf):
    print(f"buffer {buf}")


def on_disconnect(client, userdata, rc):
    print("client disconnected")


client = mqtt.Client("tls_client")
client.on_log = on_log
client.tls_set("/home/pi/iot_testbed/ca.crt")
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.connect(BROKER, PORT)
while not conn_flag:
    time.sleep(1)
    print("waiting", conn_flag)
    client.loop()
time.sleep(3)
print("publishing")
client.publish("test/tls", "hello tls!")
time.sleep(2)
client.loop()
time.sleep(2)
client.disconnect()
