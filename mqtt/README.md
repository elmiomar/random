# MQTT Install and Configuration

We will be working with the [Mosquitto Broker](http://mosquitto.org/), which is an open-source message broker that implements  the MQTT protocol versions 5.0, 3.1.1 and 3.1.

## Install Mosquitto Broker on Raspberry Pi

To install Mosquitto and its clients, run the following commands:

```
sudo apt update
sudo apt install -y mosquitto mosquitto-clients
```

If you want to make Mosquitto run on startup, use this command:

```
sudo systemctl enable mosquitto.service
```

## Test Mosquitto installation

To check if Mosquitto was successfully installed, we use its clients like this:

- start a subscriber in a terminal:

```
mosquitto_sub -v -t 'test/topic'
```

- start a publisher in another terminal:

```
mosquitto_pub -t 'test/topic' -m 'hello!'
```

You should see `test/topic hello!` appear on the subscriber's terminal.

## Mosquitto SSL Configuration - TLS Security

In this section, we configure the Mosquitto broker to use TLS security. We will use `openssl` to create the certificates. The goal is to create an encrypted connection between the broker and the clients. For that, we only need a trusted server certificate on the client.

To install `openssl`, run the following command:

```
sudo apt-get install openssl
```

Steps that we wil be performing in this guide:

- create a **Certificate Authority (CA) key pair**
- create **CA certificate** and use the **CA key** to sign it
- create a **broker key pair** without password protection
- create a **broker certificate** request using the broker key
- use the **CA certificate** to sign the broker certificate request
- move all files to a directory of choice on the broker, for example **certificates**
- get a copy of the **CA certificate** file on the client.
- edit the **Mosquitto conf** file to use the files
- edit the client to use TLS and the CA certificate

We will use the same commands as the [official page](https://mosquitto.org/man/mosquitto-tls-7.html).

Commands below:

### Move to a directory of choice

```
mkdir certificates && cd certificates
```

### Create a key pair for the Certificate Authority (CA)

```
openssl genrsa -out ca.key 2048
```

### Create a certificate for the CA using the key pair

```
openssl req -new -x509 -days 825 -key ca.key -out ca.crt
```

>**Note**: When creating a request, you will asked to provide extra information about the issuing entity. We can either enter them in interactive mode, or use the [-subject arg](https://www.openssl.org/docs/manmaster/man1/openssl-req.html). The arg must be formatted as /type0=value0/type1=value1/type2=…, characters may be escaped by \ (backslash), no spaces are skipped.


| Syntax      | Description               |
| ----------- | -----------               |
| /C=         | Country                   |
| /ST=        | State                     |
| /L=         | Location                  |
| /O=         | Organization              |
| /OU=        | Organizational Unit       |
| /CN=        | Common Name               |



Example:

```
openssl req -new -x509 -days 825 -key ca.key -out ca.crt -subj '/C=US/ST=MD/L=Gaithersburg/O=OMAR/OU=ILIAS/CN=*.elmiomar.com'
```

### Create a server key pair for the broker

```
openssl genrsa -out server.key 2048
```

### Create a certificate request

>**Note**: the common name is important, and should use the name the service will be accessed at e.g. `mqtt-broker.elmiomar.com`. IP address can be used as well, and the same name should be used on the client side when connecting to the broker. When using a name, make sure it can be resolved.

```
openssl req -new -out server.csr -key server.key
```

Example using `-subj arg`:

```
openssl req -new -out server.csr -key server.key -subj '/C=US/ST=MD/L=Gaithersburg/O=OMAR/OU=ILIAS/CN=mqtt-broker.elmiomar.com'
```

Explanation of the challenge password [here](https://serverfault.com/questions/266232/what-is-a-challenge-password).

### Create and sign the server certificate

```
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt -days 825
```

At this point, our folder should contain the following files:

- ca.crt
- ca.key
- ca.srl
- server.crt
- server.csr
- server.key

### Move files

Under `/etc/mosquitto` there are 1 folders: `certs` and `ca_certificates`.

Copy the files `server.crt` and `server.key` to `/etc/mosquitto/certs`, and copy the file `ca.crt` to `/etc/mosquitto/ca_certificates`.


```
sudo cp server.crt server.key -t /etc/mosquitto/certs
sudo cp ca.crt -t /etc/mosquitto/ca_certificates
```

### Move CA file to the client

The client should have the `ca.crt` file.

### Update Mosquitto configuration file

Mosquitto config file can be found at `/etc/mosquitto/mosquitto.conf` and a full description of the file can be found at `/usr/share/doc/mosquitto/examples/mosquitto.conf`.

Run the following command to append configuration to the Mosquitto config file.

```
sudo tee -a /etc/mosquitto/mosquitto.conf > /dev/null <<EOF
port 8883
cafile /etc/mosquitto/ca-certificates/ca.crt
keyfile /etc/mosquitto/certs/server.key
certfile /etc/mosquitto/certs/server.crt
tls_version tlsv1.2
EOF
```

>**Note**: when using TLS security, port 8883 is recommended to use. If ommitted the default port is 1883, it can be used too, however 8883 is the preferred port.

Restart Mosquitto broker:

```
sudo systemctl restart mosquitto
```

## Testing Mosquitto TLS Security

We use the paho python mqtt client to test the mosquitto broker security configuration. Following are the scripts that we used:

- Publisher code:

```python
import paho.mqtt.client as mqtt
import time

BROKER = "mqtt-broker.elmiomar.com"
PORT = 8883

# Create function for callback when client publishes a message
def on_publish(client, userdata, result):   
    print("data published")
    pass

# Create client object with client ID "publisher"
client = mqtt.Client("publisher") 
# set tls configuration, CA certificate path, and TLS version  
client.tls_set("ca.crt", tls_version=2)
# Assign function to callback for when a message is publiched
client.on_publish = on_publish
# Establish connection with the broker           
client.connect(BROKER, PORT)
# Publish a message
ret = client.publish("house/bulb1","on")  

```

- Subsriber code:

```python
import paho.mqtt.client as mqtt

BROKER = "mqtt-broker.elmiomar.com"
PORT = 8883

# The callback for when the client connects to the broker
def on_connect(client, userdata, flags, rc):  
    # Print result of connection attempt
    print(f"Connected with result code {rc}"))  
    # Subscribe to the topic “house/bulb1”, receive any messages published on it
    client.subscribe("house/bulb1")  

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg): 
    # Print the received msg
    print(f"Message received {msg.topic} -> {str(msg.payload)}")  

# Create instance of client with client ID “subscriber”
client = mqtt.Client("subscriber")  
client.tls_set("ca.crt", tls_version=2)
# Assign callback function for successful connection
client.on_connect = on_connect 
# Assign callback function to receive a message
client.on_message = on_message  
# Establish connection with the broker  
client.connect(BROKER, PORT)
# Start loop
client.loop_forever()  
```


>**Note**: The `ca.crt` is the CA certificate that was generated on the broker. The client must have a copy of it. Don't forget to add name resolution to `/etc/hosts`.











