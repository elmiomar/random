# VS Code for Python

VS Code setup for Python development.

## Download

Go on the [official website](https://code.visualstudio.com/) and follow steps for your OS.


## Extensions

Below is a list of useful and interesting extensions that can make your Python development easier while using VS Code.

#### How to install Extensions

VS Code contains a minimal setup out of the box, and extensions let you add extra features, tools, programming languages, debugging, etc. to empower your workflow.

This thorough [guide](https://code.visualstudio.com/docs/editor/extension-gallery) explains how to find, install, and manage VS Code extensions from the Visual Studio Code Marketplace.

#### Python by Microsoft

The [Python extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-python.python) enables a lot of features like intellicense, debugging, linting, code formatting and much more. You can select the Python version that you want.


#### Python Docstring Generator

The [Python Docstring Generator](https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring) allows to quickly generate documentation for your Python code. It supports multiple Docstring formats such as Google (default), Numpy, and Sphinx.

It is recommended to fully implement the function first, then generate the Docstring.

#### Python Indent

The [Python Indent extension](https://marketplace.visualstudio.com/items?itemName=KevinRose.vsc-python-indent) manages indentation while writing Python code.


#### Code Runner

The [Code Runner extension](https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner) allows to instantly run the code in the text editor. It supports multiple programming languages including Python.

To run the code:
- use shortcut `Ctrl+Alt+N`
- or press `F1` and then select/type `Run Code`

#### Code Linter

VS Code allows you to select a linter for our code. To choose a linter, open the command palette `Ctrl + Shift + P` for Windwos or `CMD + Shift + P`, then type "linter". A list appears and you can choose which linter you want to use. You might has to install the chosen linter module if it's not already installed. A message will appear after you choose a linter and it will ask you to install it.

![linter_popup](img/python_linter_install_popup.PNG "Linter Install Message")

