

## Comparison between Docs in repo vs Wiki:

Writing good documentation is important when developing software as a team. There are many tools for documenting the code out there, the most common options in the open source world are: using markdown/rst files in a `docs` folder inside the repo, or using the `wiki` page. Following are the pros and cons of each option:

### Using docs files

#### Pros
- Documentation changes can be tracked alongside the code
- Quick and simple way to document the code
- Enables peer review and pull requests
- Easy to add links and images to the markdown files

#### Cons
- No separation of concerns, the documentation lives with code, which can be cumbersome
- Can be hard to navigate (many folders, subfolders, and files)
  

### Using the Wiki

#### Pros

- Uses markdown/rst same as for README and docs files
- Easy navigation through the table of content
- Separates the code and the doc, but still tied to the same reposiotry

#### Cons

- No pull requests, hence not easy to peer review which might cause incorrect documentation 

One way to allow wiki peer review is to create a second repository that hosts the wiki of the projects repo. With some CI/CD setup pushing the changes to this repo will automatically update the original wiki.

The main con of this option is that we will have to maintain an extra repo.

### Examples
- [D3](https://github.com/d3/d3/wiki)
- [Netflix/Hystrix](https://github.com/Netflix/Hystrix/wiki)
